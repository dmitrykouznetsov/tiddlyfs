all: test install clean

install:
	@pip install -e .

test:
	@python -m pytest --disable-warnings -v \
		--cov=. tests.py

clean:
	@python setup.py clean
	@rm -rf *.egg-info dist build
	@rm -rf __pycache__

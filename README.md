# Tiddlywiki filesystem

Tiddlywiki allows for maximum intertwiddlyness by having external links to the file system.
A powerful extension would be to use this approach to handling data in the filesystem.
The `tiddlyfs` tool is made to simplify storing files in a central location and prividing a template for a tiddle derived from the file metadata.

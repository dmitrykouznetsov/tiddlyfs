import pytest
import os
import json
from click.testing import CliRunner
from tiddlyfs import calculate_hash, cli, wiki_entry


@pytest.fixture
def dummyfile_hash():
    return "da39a3ee5e6b4b0d3255bfef95601890afd80709"


def test_hash(dummyfile_hash):
    assert calculate_hash("testfile") == dummyfile_hash, \
            "SHA1 should match"


def test_metadata(dummyfile_hash):
    tag = "testing"
    _, tiddler = wiki_entry("testfile")

    # Creation time will not be the same on all platforms
    assert tiddler.find("pre").string \
        == "[ext[Open file|file:./kdb/" + dummyfile_hash + "]]"


def test_none_file():
    result = CliRunner().invoke(cli, ["-d", ".", "unicorn"])
    assert result.output == "[Errno 2] No such file or directory: 'unicorn'\n"

def test_none_wiki():
    result = CliRunner().invoke(cli, ["-d", ".", "-w", "wiki", "testfile"])
    assert result.output == "[Errno 2] No such file or directory: 'wiki'\n"

def test_move_void():
    result = CliRunner().invoke(cli, ["--database", "blackhole", "testfile"])
    assert result.output == "[Errno 2] No such file or directory: 'blackhole'\n"

#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright 2020 Dmitry Kouznetsov <dmitry.kouznetsov@protonmail.com>
#
# This is free software: you can redistribute it
# and/or modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 2 of the
# License, or (at your option) any later version.
#
# This is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program. If not, see http://www.gnu.org/licenses/.
#
import click
import os, sys
import hashlib
import shutil
from bs4 import BeautifulSoup
from time import strftime
import errno


def wiki_entry(file):
    """Generate a filepath and tiddlywiki entry for a file"""
    sha1 = calculate_hash(file)
    extension = os.path.splitext(file)[1]
    timestamp = strftime("%Y%m%d%H%M%S")

    soup = BeautifulSoup(features="html.parser")
    tiddler = soup.new_tag("div",
	# Make it easy to search for the file in the Tiddlywiki
        title="File",

        # Default tag
        tags="file",

        # Let the tiddle show up in recent tiddles
        created=timestamp,
        modified=timestamp,

        # Only this type of tiddle works with dynamic links in tiddlywiki
        type="text/vnd.tiddlywiki")

    # The actual content is always wrapped in an additional tag
    content = soup.new_tag("pre")

    # TODO: Remove the dependency. Note that these links must be
    # synced with TiddlyWiki somehow
    hashed_filename = sha1 + extension if extension else sha1
    content.string = f"[ext[Open file|file:./kdb/{hashed_filename}]]"

    tiddler.append(content)
    return hashed_filename, tiddler


def calculate_hash(file):
    sha1sum = hashlib.sha1()
    path = os.path.abspath(file)

    with open(path, 'rb') as source:
        block = source.read(2**16)
        while len(block) != 0:
            sha1sum.update(block)
            block = source.read(2**16)

    return sha1sum.hexdigest()


def verify(location):
    if not os.access(location, os.R_OK):
        err = FileNotFoundError(errno.ENOENT, os.strerror(errno.ENOENT), location)
        print(err)
        sys.exit(err.errno)


@click.command()
@click.option("-d", "--database", envvar="TIDDLYDB", \
        help="Target database directory (env TIDDLYDB)")
@click.option("-w", "--wiki", envvar="TIDDLYWIKI", \
        help="Location of the tiddly wiki file (env TIDDLYWIKI)")
@click.option("-m", "--move", is_flag=True)
@click.argument("file")
def cli(database, wiki, move, file):
    # Check if all endpoints exists at the beginning and exit if exception
    for location in (database, wiki, file):
        verify(location)

    hashed_filename, tiddler = wiki_entry(file)

    if not move:
        move = click.confirm("Move the file to the database folder?", default=True)

    if move:
        # Compose the modified TiddlyWiki html
        with open(wiki, "r") as html:
            soup = BeautifulSoup(html, features="html.parser")

            # Tiddlywiki has a fixed div where entries should be stored
            lib = soup.find(id="storeArea")
            lib.insert(0, tiddler)

        try:
            shutil.move(file, os.path.join(database, hashed_filename))

            # At this stage it is safe to add the tiddler tag to the tiddlywiki
            with open(wiki, "w") as html:
                html.write(str(soup))

        except Exception as e:
            print(e)
            sys.exit()

    # TODO: Make clear when a file has been moved
    print("Generated TiddlyWiki entry: ", end="\n\n")
    print(tiddler.prettify(), end="\n\n")

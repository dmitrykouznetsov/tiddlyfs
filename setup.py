from setuptools import setup

setup(
    name="tiddlyfs",
    version="0.1",
    description="CLI tool to extract a tiddly file entry from a file",
    url="https://gitlab.com/dmitrykouznetsov/tiddlyfs",
    author="Dmitry Kouznetsov",
    license="GPLv2",
    packages=[],
    install_requires=["click", "beautifulsoup4"],

    # The extras can be installed with `pip install -e .[test]`
    extras_require={

        # Less testing overload by checking types
        "test": ["mypy", "pytest", "pytest-cov"],
    },

    # This part is specific for the click library
    entry_points="""
    [console_scripts]
    tiddlyfs=tiddlyfs:cli
    """,

    # Slight performance boost
    zip_safe=True)
